package com.example.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class Settings extends AppCompatActivity implements View.OnClickListener {

    public Switch SwitchofNightMode;
    private ActionBar actionBar;
    private RelativeLayout change_data;


    public Settings() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        actionBar = getSupportActionBar();
        actionBar.show();
        //显示返回箭头
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("设置");
        change_data = findViewById(R.id.change_data);
        change_data.setOnClickListener(this);

        //夜间模式
        SwitchofNightMode = findViewById(R.id.SwitchofNightMode);

        int currentMode = AppCompatDelegate.getDefaultNightMode();

        if (currentMode == AppCompatDelegate.MODE_NIGHT_YES) {
            SwitchofNightMode.setChecked(true);
        } else {
            SwitchofNightMode.setChecked(false);
        }

        SwitchofNightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    restartCurrentActivity();
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    restartCurrentActivity();
                }
            }
        });
        //夜间模式结束

        //消息通知开关
    }

    public Switch getSwitchofNightMode() {
        return this.SwitchofNightMode;
    }

    //重新启动
    private void restartCurrentActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:   //返回键的id
                this.finish();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_data:
                Intent intent = new Intent(Settings.this,ChangeInformation.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

}