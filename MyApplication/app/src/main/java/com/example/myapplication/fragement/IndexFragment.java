package com.example.myapplication.fragement;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.myapplication.DemoAdapter;
import com.example.myapplication.R;
import com.example.myapplication.api.comment.get_comment;
import com.example.myapplication.api.content.get_list;

import java.util.ArrayList;


public class IndexFragment extends Base {
    public ExpandableListView mExpandableListView;
    private ArrayList<ArrayList<String>> group0,group;
    private String[][] mChildList = new String[][]{{"评论者1", "我是大笨蛋"}, {"评论者2", "我才是大笨蛋！"}};;
    private ArrayList<ArrayList<String>> child0,child;
    DemoAdapter demoAdapter;
    private int num=0, n=0 ;
    private Boolean is_loading = true;  //判断是否翻页

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_index, null, false);
        mExpandableListView = (ExpandableListView) view.findViewById(R.id.expandablelistview);
//        View foot = inflater.inflate(R.layout.foot_boot,null,false);
//        foot.findViewById(R.id.load_layout).setVisibility(View.GONE);
//        mExpandableListView.addFooterView(foot);
        progetDate();   // 加载默认数据
        showListView();  // 显示

        // 上滑刷新
        final SwipeRefreshLayout swip_refresh_layout=view.findViewById(R.id.swipeLayout);
        // 设置转圈圈颜色-支持三色变化-转一圈变一色
        swip_refresh_layout.setColorSchemeResources(R.color.purple_200,R.color.purple_500,R.color.purple_700);
        // 设置背景颜色-默认白色
        //swip_refresh_layout.setProgressBackgroundColorSchemeColor(R.color.black);
        swip_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("","上滑");
                // 模拟耗时操作
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 进行逻辑数据重载
                        progetDate();   // 加载默认数据
                        demoAdapter = new DemoAdapter(group, mChildList, getContext());
                        mExpandableListView.setAdapter(demoAdapter);
                        // 将下拉动画取消
                        swip_refresh_layout.setRefreshing(false);
                    }
                },2000);
            }
        });

        // 下滑分页
        mExpandableListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    // 当不滚动时
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 判断滚动到底部
                        Boolean flag = isListViewReachBottomEdge(view);
                        Log.e("flag",""+flag);
                        if(flag == true){
                            Toast.makeText(getContext(), "加载中...", Toast.LENGTH_SHORT).show();
                            Handler handler = new Handler();
                            //模拟一个延迟两秒的刷新功能
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //开始加载更多数据
                                    loadMoreData();
                                    showListView();  // 响应
                                    Log.e("页数","第--"+num+"--页");
                                }
                            },200);
                        }
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {}
        });

        // 一级点击监听
        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                //如果你处理了并且消费了点击返回true,这是一个基本的防止onTouch事件向下或者向上传递的返回机制
                return false;
            }
        });
        // 全部item展开
        int groupCount = mExpandableListView.getCount();
        for (int i=0; i<groupCount; i++) {
            mExpandableListView.expandGroup(i);
        };

        // 二级点击监听
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                //如果你处理了并且消费了点击返回true
                return false;
            }
        });

        return view;
    }

    private boolean isListViewReachBottomEdge(final AbsListView listView) {
        boolean result = false;
        if (listView.getLastVisiblePosition() == (listView.getCount() - 1)) {
            final View bottomChildView = listView.getChildAt(listView.getLastVisiblePosition() - listView.getFirstVisiblePosition());
            result = (listView.getHeight() >= bottomChildView.getBottom());
        };
        return result;
    }

    private void progetDate() {
        list(0);
        this.group = this.group0;
        child0 = new ArrayList<ArrayList<String>>();
//        for(int j=0;j<10;j++){
//            get_comment g_c = new get_comment(group0.get(j).get(0));
//            while(g_c.sign==0) {
//                if (g_c.sign != 0) {break;}
//            }
//            Log.e("====",""+g_c.group);
//
////            if(g_c.sign==200 && g_c.group.size() != 0){
////
////            }
//            if()
//                for(int i=0;i<Integer.valueOf(g_c.group.size());i++){
//                    String na_me = g_c.group.get(i).get(2);
//                    String con_tent = g_c.group.get(i).get(1);
//                    ArrayList<String> zzchild = new ArrayList<String>();
//                    zzchild.add(na_me);
//                    zzchild.add(con_tent);
//                    child0.add(zzchild);
//                }
//            }
//            child = child0;
//            Log.e("--------child",""+child);
    }

    private void showListView() {
        if(demoAdapter == null){
            demoAdapter = new DemoAdapter(group, mChildList, this.getActivity());
            mExpandableListView.setAdapter(demoAdapter);
        }else{
            demoAdapter.notifyDataSetChanged();

        }
        Log.e("group长度",""+group.size());
    }

    /**
     * 开始加载更多新数据，这里每次只更新5条数据
     */
    private void loadMoreData() {
        if(n==0){
            if(is_loading){
                num++;
                Log.e("第几页-",""+num);
                list(num);
                is_loading = false;
                for(int i=0;i<5;i++){
                    group.add(group0.get(i));

                }
                Log.e("0000000000","000000");
            }
        }else{
            for(int i=5;i<10;i++){
                group.add(group0.get(i));
            }
            Log.e("111111111","111111");
        }
        n+=5;
        if(n==10){
            n=0;
            is_loading = true;
        }
        Log.e("n",n+"-"+is_loading);
    }

    // 获取group列表数据
    private void list(int num) {
        get_list g = new get_list(num);
        this.group0 = g.get_groupList();
        Log.e("list-num",""+num);
    }



}