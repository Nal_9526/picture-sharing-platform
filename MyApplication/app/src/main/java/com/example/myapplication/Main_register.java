package com.example.myapplication;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.api.login;
import com.example.myapplication.api.register;

public class Main_register extends AppCompatActivity {
    private EditText name,pas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        name = (EditText) findViewById(R.id.num);
        pas = (EditText) findViewById(R.id.password);
        Button btnlogin = (Button) findViewById(R.id.btn_login);
        Button btnregister = (Button) findViewById(R.id.btn_register);
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = name.getText().toString();
                String password = pas.getText().toString();
                register r = new register(username,password);
                r.post();

                while(r.sign==0&&r.sign2==null) {
                    if (r.sign != 0 && r.sign2 != null) {break;}
                }
                name.setText("");
                pas.setText("");
                if(r.sign==200){
                    Intent intent = new Intent(Main_register.this, MainActivity.class);
                    startActivity(intent);
                }else if (r.sign2.equals("用户名已存在")){
                    error1();
                }else{
                    error();
                }

            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Main_register.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void error(){
        new AlertDialog.Builder(this)
                .setTitle("错误")
                .setMessage("密码输入错误")
                .setPositiveButton("重新输入",null)
                .show();
    }
    private void error1(){
        new AlertDialog.Builder(this)
                .setMessage("用户名已存在")
                .setPositiveButton("重新输入",null)
                .show();
    }

}
