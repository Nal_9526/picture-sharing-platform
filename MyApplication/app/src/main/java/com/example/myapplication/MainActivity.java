package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.api.login;

public class MainActivity extends AppCompatActivity {
    private EditText name;
    private EditText pas;
    public static String id=null;
    public static String user_name;
    public static String introduce;
    public static String avatar;
    public static String lastUpdateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnlogin = (Button) findViewById(R.id.btn_login);
        Button btnregister = (Button) findViewById(R.id.btn_register);
        name = (EditText) findViewById(R.id.num);
        pas = (EditText) findViewById(R.id.password);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = name.getText().toString();
                String password = pas.getText().toString();
                if(username==null){
                    Toast.makeText(getApplicationContext(), "昵称不能为空哦~", Toast.LENGTH_SHORT);
                }else {
                    Toast.makeText(getApplicationContext(), "密码不能为空哦~", Toast.LENGTH_SHORT);
                }
                login l = new login(username,password);
                l.post();
                  // 取消过度等待
//                while(l.sign==0) {
//                    if (l.sign != 0) {break;}
//                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(l.sign==200){
                    id = l.id;
                    user_name = l.name;
                    introduce = l.introduce;
                    avatar = l.avatar;
                    lastUpdateTime = l.lastUpdateTime;
                    Intent intent = new Intent(MainActivity.this, FunctionActivity.class);
                    startActivity(intent);
                    Log.e("----","登录成功");
                }else{
                    error();
                    Log.e("----","登录失败");
                }
            }
        });
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Main_register.class);
                startActivity(intent);
            }
        });
    }

    private void error(){
        new AlertDialog.Builder(this)
                .setTitle("错误")
                .setMessage("密码输入错误")
                .setPositiveButton("重新输入",null)
                .show();
    }

}