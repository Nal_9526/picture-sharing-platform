package com.example.myapplication.fragement;


import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.ChangeInformation;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.Settings;
import com.example.myapplication.home.Follow;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**

 */
public class HomeFragment extends Base implements View.OnClickListener {
    View view;
    ViewHolder viewHolder;
    private ArrayList<String> userinfo;
    private Context context = getContext();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        init(); //初始化
        get_userinfo();

        return view;
    }

    private void get_userinfo() {
        // 时间戳转换成时间
        Long time = Long.valueOf(MainActivity.lastUpdateTime);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sd = sdf.format(time);
        if(MainActivity.avatar!=null){
            Picasso.get().load(MainActivity.avatar)
                    .fit()
                    .centerCrop()
                    .into(viewHolder.f_home_image);
        }
        if(viewHolder.f_home_text1!=null){
            viewHolder.f_home_text1.setText(MainActivity.user_name);
        }
        if(viewHolder.f_home_text2!=null){
            viewHolder.f_home_text2.setText(MainActivity.introduce);
        }
        if(viewHolder.login_time!=null){
            viewHolder.login_time.setText(sd);
        }
    }

    @SuppressLint("Range")
    private String getPath(Context context, Uri uri) {
        String path = null;
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            return null;
        }
        if (cursor.moveToFirst()) {
            try {
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        cursor.close();
        return path;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.f_home_image:
                Toast.makeText(getActivity(), "查看大头像", Toast.LENGTH_SHORT).show();
                break;
            case R.id.f_home_text1:
                Toast.makeText(getActivity(), "昵称", Toast.LENGTH_SHORT).show();
                break;
            case R.id.f_home_text2:
                Toast.makeText(getActivity(), "ID", Toast.LENGTH_SHORT).show();
                break;
            case R.id.f_home_1:
                Toast.makeText(getActivity(), "我的发布", Toast.LENGTH_SHORT).show();
                break;
            case R.id.f_home_2:
                Toast.makeText(getActivity(), "我的关注", Toast.LENGTH_SHORT).show();
                break;
            case R.id.f_home_3:
                Toast.makeText(getActivity(), "版本更新", Toast.LENGTH_SHORT).show();
                break;
            case R.id.f_home_4:
                Intent intent = new Intent(this.getActivity(), Settings.class);
                startActivity(intent);
                Toast.makeText(getActivity(), "详细设置", Toast.LENGTH_SHORT).show();
                break;
            case R.id.f_home_5:
                Toast.makeText(this.getActivity(), "我的收藏", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(this.getActivity(), Follow.class);
                startActivity(intent1);
                break;
            default:
                break;
        }
    }

    private void init(){
        viewHolder = new ViewHolder();
        viewHolder.f_home_image =  view.findViewById(R.id.f_home_image);
        viewHolder.f_home_text1 =  view.findViewById(R.id.f_home_text1);
        viewHolder.f_home_text2 =  view.findViewById(R.id.f_home_text2);
        viewHolder.login_time =  view.findViewById(R.id.login_time);
        viewHolder.f_home_1 = view.findViewById(R.id.f_home_1);
        viewHolder.f_home_2 = view.findViewById(R.id.f_home_2);
        viewHolder.f_home_3 = view.findViewById(R.id.f_home_3);
        viewHolder.f_home_4 = view.findViewById(R.id.f_home_4);
        viewHolder.f_home_5 = view.findViewById(R.id.f_home_5);

        viewHolder.f_home_image.setOnClickListener(this);
        viewHolder.f_home_text1.setOnClickListener(this);
        viewHolder.f_home_text2.setOnClickListener(this);
        viewHolder.f_home_1.setOnClickListener(this);
        viewHolder.f_home_2.setOnClickListener(this);
        viewHolder.f_home_3.setOnClickListener(this);
        viewHolder.f_home_4.setOnClickListener(this);
        viewHolder.f_home_5.setOnClickListener(this);
    }

    class ViewHolder{
        ImageView f_home_image;
        TextView f_home_text1,f_home_text2,login_time;
        LinearLayout f_home_1,f_home_2,f_home_3,f_home_4,f_home_5;
    }
}