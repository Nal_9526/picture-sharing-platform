package com.example.myapplication.api_basic;

/**
 * **********step1**********
 * 网络安全配置
 * 在资源文件新建一个xml目录，在该目录下新建文件network_security_config.xml，配置如下
 * <?xml version="1.0" encoding="utf-8"?>
 * <network-security-config >
 *     <base-config cleartextTrafficPermitted="true" />
 * </network-security-config >
 *
 * **********step2**********
 * 在AndroidManifest.xml的<application></application>标签中引入上一步所添加的网络配置相关资源文件
 * <application android:networkSecurityConfig="@xml/network_security_config"></application>
 *
 * **********step3**********
 * 在AndroidManifest.xml中添加如下配置添加网络请求权限
 * <uses-permission android:name="android.permission.INTERNET" />
 *
 * **********step4**********
 * 添加以下依赖到build.gradle，用户可自主在[https://mvnrepository.com]仓库中选择合适的版本
 *   // 网络请求框架 okhttp3
 *   implementation 'com.squareup.okhttp3:okhttp:3.10.0'
 *   //用来解析json串
 *   // https://mvnrepository.com/artifact/com.google.code.gson/gson
 *   implementation 'com.google.code.gson:gson:2.9.1'
 *
 * **********step5**********
 * 用户对所请求的数据进行自主操作
 */

public class Basic {
    // 请求密钥
    public static String appId="603ed4bb2b7f4594b8f90eb0f4d12209";
    public static String appSecret="429616e8e923ba9c64268bb02ddc9e7825109";

    public void get_userid(){

    }

}
