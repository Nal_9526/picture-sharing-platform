package com.example.myapplication.fragement;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.myapplication.FunctionActivity;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.api.add.img_upload;
import com.example.myapplication.api.add.publish;
import com.example.myapplication.api.add.save;
import com.example.myapplication.api.content.TexImg_add;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**

 */
@SuppressLint("ValidFragment")
public class AddFragment extends Base{
    /** 以下为需要请求的信息*/
    //内容
    private Context context;
    private String content;
//    //主键id
//    private Integer id = 100;
    //imageCode:一组图片的唯一标识符（需要先通过上传文件获取）
    private String imageCode;
    private String[] imgList;
    //当前登录用户（发布者）id
    private String pUserId;
    //标题
    private String title;

    private static EditText f_title;
    private static EditText f_add_text;
    private static ImageView img;
    private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
    private static final int PHOTO_REQUEST_CUT = 3;// 结果
    private File F;

    private Button btn,btn_add;
    private int find=0;


    //无参构造方法
    @SuppressLint("ValidFragment")
    public AddFragment(Context context){
        this.context = context;
    }

    //带参的构造方法
    @SuppressLint("ValidFragment")
    public AddFragment(String imageCode){
        this.imageCode = imageCode;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.f_title = (EditText)this.getActivity().findViewById(R.id.f_title);
        this.f_add_text = (EditText)this.getActivity().findViewById(R.id.f_add_text);

        this.img = (ImageView) this.getActivity().findViewById(R.id.image);
        btn = getActivity().findViewById(R.id.f_add);
        btn_add = getActivity().findViewById(R.id.btn_add);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("AddFragment","");
                gallery();
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("AddFragment","点击提交");
                while (find==0){
                    if(find != 0 ){break;}
                }
                //获取标题
                title = f_title.getText().toString();
                //获取文本描述内容
                content = f_title.getText().toString();
                pUserId = MainActivity.id;
                Log.i("pUserId-imageCode",""+pUserId+"-"+imageCode);
                //新增
                TexImg_add texImg_add = new TexImg_add(content, imageCode, pUserId, title);
                    texImg_add.post();
                while (texImg_add.found_0 == 0){
                    if(texImg_add.found_0 != 0){break;}
                }
                Log.e("texImg_add","新增成功-0");
                //保存
                save save = new save(content,imageCode,pUserId,title);
                    save.post();
                while (save.found_1 == 0){
                    if(save.found_1 != 0){break;}
                }
                Log.e("save","保存成功-1");
                //发布
                publish publish = new publish(content, imageCode, pUserId, title);
                    publish.post();
                Log.e("publish","发表成功-2");
                tip_success();
            }
        });
    }

    /*
     * 从相册获取
     */
    public void gallery() {
        // 激活系统图库，选择一张图片
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        //全文件搜索
//        intent.setAction(Intent.ACTION_GET_CONTENT);
        // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_GALLERY
        startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
    }
    /*
     * 剪切图片
     */
    private void crop(Uri uri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 可裁剪
//        // 裁剪框的比例，1：1
//        intent.putExtra("aspectX", 1);
//        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 400);
        intent.putExtra("outputY", 400);

        intent.putExtra("outputFormat", "JPEG");// 图片格式
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);
        // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CUT
        startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PHOTO_REQUEST_GALLERY) {
            // 从相册返回的数据
            if (data != null) {
                // 得到图片的全路径
                Uri uri = data.getData();
                Log.e("uri",""+uri);
                crop(uri);

                if (Build.VERSION.SDK_INT >= 23) {
                    int REQUEST_CODE_CONTACT = 101;
                    String[] permissions = {
                            Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    //验证是否许可权限
                    for (String str : permissions) {
                        if (getActivity().checkSelfPermission(str) != PackageManager.PERMISSION_GRANTED) {
                            //申请权限
                            getActivity().requestPermissions(permissions, REQUEST_CODE_CONTACT);
                            return;
                        } else {
                            //这里就是权限打开之后自己要操作的逻辑

                            // 将url转为文件格式
                            String[] proj = { MediaStore.Images.Media.DATA };
                            Cursor actualimagecursor = getActivity().managedQuery(uri,proj,null,null,null);
                            int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                            actualimagecursor.moveToFirst();
                            String img_path = actualimagecursor.getString(actual_image_column_index);
                            File Fi = new File(img_path);
                            Uri fileUri = Uri.fromFile(Fi);
                                Log.e("Fi-",""+Fi);
                                Log.e("fileUri-",""+fileUri);
                            F = Fi;

                        }
                    }
                }


            }

        } else if (requestCode == PHOTO_REQUEST_CUT) {
            // 从剪切图片返回的数据
            if (data != null) {
                Bitmap bitmap = data.getParcelableExtra("data");
                this.img.setImageBitmap(bitmap);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        initview();
    }

    public void initview() {
        ArrayList<File> fileList = new ArrayList<>();
        fileList.add(F);

        img_upload upload = new img_upload(fileList);
        upload.post();
        while(upload.sign==0) {
            if (upload.sign != 0) {break;}
        }
        if(upload.sign==200){
            this.imageCode = upload.imgCode;
            this.imgList = upload.imgList;
        }
        find = 1;
    }

    private void tip_success(){
        new AlertDialog.Builder(context)
                .setTitle("Tip")
                .setMessage("添加成功哦~~")
                // 清空
                .setPositiveButton("好哦哥哥",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clean();
                    }
                })
                .show();
    }

    public static void clean(){
        f_title.setText("");
        f_add_text.setText("");
        img.setImageDrawable(null);
    }

}
