package com.example.myapplication.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.api.user.user_follow;

import java.util.ArrayList;

public class Follow extends AppCompatActivity {
    private ExpandableListView eplistview;
    private Follow_Adapter follow_adapter;
    private Context context = Follow.this;
    private ArrayList<ArrayList<String>> group=null,group0=null;
    private String[][] mChildList = new String[][]{{"评论者1", "我是大笨蛋"}, {"评论者2", "我才是大笨蛋！"}};;
    private int num=0, n=0 ;
    private Boolean is_loading = true;  //判断是否翻页
    private String userId = MainActivity.id;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_follow);

        actionBar = getSupportActionBar();
        actionBar.show();
//        actionBar.setDisplayUseLogoEnabled(false);
        //显示返回箭头
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("返回-Home");

        eplistview = findViewById(R.id.eplistview);
        progetDate();   // 加载默认数据
        if(group == null){
            setContentView(R.layout.follow_empty);
        }else{
            showListView();  // 显示
            listener();    // 监听
        }
    }

    private void listener(){
        // 上滑刷新
        final SwipeRefreshLayout home_follow = findViewById(R.id.home_follow);
        // 设置转圈圈颜色-支持三色变化-转一圈变一色
        home_follow.setColorSchemeResources(R.color.purple_200, R.color.purple_500, R.color.purple_700);
        // 设置背景颜色-默认白色
        //swip_refresh_layout.setProgressBackgroundColorSchemeColor(R.color.black);
        home_follow.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("", "上滑");
                // 模拟耗时操作
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 进行逻辑数据重载
                        progetDate();   // 加载默认数据
                        if(group == null) {
                            setContentView(R.layout.follow_empty);
//                            Intent intent = new Intent(Follow.this, Follow_empty.class);
//                            startActivity(intent);
                        }else{
                            follow_adapter = new Follow_Adapter(group, mChildList, context);
                            eplistview.setAdapter(follow_adapter);
                        }
                        // 将下拉动画取消
                        home_follow.setRefreshing(false);
                    }
                }, 2000);
            }
        });
        // 下滑分页
        eplistview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    // 当不滚动时
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 判断滚动到底部
                        Boolean flag = isListViewReachBottomEdge(view);
                        Log.e("flag", "" + flag);
                        if (flag == true) {
                            Toast.makeText(context, "加载中...", Toast.LENGTH_SHORT).show();
                            Handler handler = new Handler();
                            //模拟一个延迟两秒的刷新功能
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //开始加载更多数据
                                    loadMoreData();
                                    if(group == null) {
                                        setContentView(R.layout.follow_empty);
//                                        Intent intent = new Intent(Follow.this, Follow_empty.class);
//                                        startActivity(intent);
                                    }else{
                                        showListView();  // 响应
                                        Log.e("页数", "第--" + num + "--页");
                                    }
                                }
                            }, 200);
                        }
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
            }
        });
        // 一级点击监听
        eplistview.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                //如果你处理了并且消费了点击返回true,这是一个基本的防止onTouch事件向下或者向上传递的返回机制
                return false;
            }
        });
        // 全部item展开
//        int groupCount = eplistview.getCount();
//        for (int i=0; i<groupCount; i++) {
//            eplistview.expandGroup(i);
//        };

        // 二级点击监听
        eplistview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                //如果你处理了并且消费了点击返回true
                return false;
            }
        });
    }

    private boolean isListViewReachBottomEdge(final AbsListView listView) {
        boolean result = false;
        if (listView.getLastVisiblePosition() == (listView.getCount() - 1)) {
            final View bottomChildView = listView.getChildAt(listView.getLastVisiblePosition() - listView.getFirstVisiblePosition());
            result = (listView.getHeight() >= bottomChildView.getBottom());
        };
        return result;
    }

    private void progetDate() {
        list(0);
        this.group = this.group0;
    }

    private void showListView() {
        if(follow_adapter == null){
            follow_adapter = new Follow_Adapter(group, mChildList, context);
            eplistview.setAdapter(follow_adapter);
        }else{
            follow_adapter.notifyDataSetChanged();

        }
    }

    /**
     * 开始加载更多新数据，这里每次只更新5条数据
     */
    private void loadMoreData() {
        if(n==0){
            if(is_loading){
                num++;
                list(num);
                is_loading = false;
                for(int i=0;i<5;i++){
                    group.add(group0.get(i));
                }
            }
        }else{
            for(int i=5;i<10;i++){
                group.add(group0.get(i));
            }
        }
        n+=5;
        if(n==10){
            n=0;
            is_loading = true;
        }
        Log.e("n",n+"-"+is_loading);
    }

    // 获取group列表数据
    private void list(int num) {
        user_follow g = new user_follow(num+"",userId);
        this.group0 = g.get_info();
        Log.e("user_follow-num",""+num);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:   //返回键的id
                this.finish();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
