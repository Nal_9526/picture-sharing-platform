package com.example.myapplication.api.add;

import static com.example.myapplication.api_basic.Basic.appId;
import static com.example.myapplication.api_basic.Basic.appSecret;

import androidx.annotation.NonNull;
import android.os.NetworkOnMainThreadException;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class img_upload {
    private final Gson gson = new Gson();
    private ArrayList<File> fileList;

    public static String imgCode;
    public static String[] imgList;
    public static int sign=0;

    public img_upload(ArrayList<File> fileList){
        this.fileList = fileList;
    }

    public void post(){
        new Thread(() -> {

            // url路径
            String url = "http://47.107.52.7:88/member/photo/image/upload";

            // 请求头
            Headers headers = new Headers.Builder()
                    .add("Accept", "application/json, text/plain, */*")
                    .add("appId", appId)
                    .add("appSecret", appSecret)
                    .add("Content-Type", "multipart/form-data")
                    .build();


//            MultipartBody.Builder requestBody = new MultipartBody.Builder()
//                                                                 .setType(MultipartBody.FORM);
//            RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), fileList.get(0));
//                Log.e("img_upload--body",""+fileList.get(0));
//            String filename = fileList.get(0).getName();
//                Log.e("img_upload--filename",""+filename);
//            requestBody.addFormDataPart("fileList", "fileList", body);
//            Request request = new Request.Builder()
//                                         .url(url)
//                                           .headers( headers)
//                                         .post(requestBody.build())
//                                         .build();
//                Log.e("img_upload--request",""+request);

//            File file=new File("/storage/emulated/0/Pictures/IMG_20220919_151918.jpg");//文件路径(本地)
            File file = fileList.get(0);
            MultipartBody.Builder requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM);
            RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                Log.e("img_upload--body",""+file);
            String filename = file.getName();
                Log.e("img_upload--filename",""+filename);
            requestBody.addFormDataPart("fileList", filename, body);
                Log.e("img_upload--requestBody.addFormDataPart",""+requestBody.addFormDataPart("fileList", filename, body));
            Request request = new Request.Builder()
                    .url(url)
                    .headers( headers)
                    .post(requestBody.build())
                    .build();
                Log.e("img_upload--request",""+request);

//            MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

//            //请求组合创建
//            Request request = new Request.Builder()
//                    .url(url)
//                    // 将请求头加至请求中
//                    .headers(headers)
//                    .post(RequestBody.create(MEDIA_TYPE_JSON,  body))
//                    .build();
            try {
                OkHttpClient client = new OkHttpClient();
                Log.e("img_upload--client",""+client);
                //发起请求，传入callback进行回调
                client.newCall(request).enqueue(callback);
            }catch (NetworkOnMainThreadException ex){
                ex.printStackTrace();
            }
        }).start();
    }

    /**
     * 回调
     */
    private final Callback callback = new Callback() {
        @Override
        public void onFailure(@NonNull Call call, IOException e) {
            //TODO 请求失败处理
            Log.e("img_upload--callback,onFailure",""+call+"--"+e);
            e.printStackTrace();
        }
        @Override
        public void onResponse(@NonNull Call call, Response response) throws IOException {
            //TODO 请求成功处理
            Type jsonType = new TypeToken<ResponseBody<Object>>(){}.getType();
            // 获取响应体的json串
            String body = response.body().string();
            Log.d("img_upload--info", body);
            // 解析json串到自己封装的状态
            ResponseBody<Object> dataResponseBody = gson.fromJson(body,jsonType);
            parseJSONWithJSONObject(body);
            Log.d("img_upload--info", dataResponseBody.toString());
        }
    };

    public void parseJSONWithJSONObject(String jsonData) {//用JSONObect解析JSON数据
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                Log.d("img_upload----data",""+jsonObject1);
            imgCode = jsonObject1.getString("imageCode");
            JSONArray imgUrlList = jsonObject1.getJSONArray("imageUrlList");
            imgList = new String[imgUrlList.length()];
            imgUrlList.getString(0);
            for(int i=0;i<imgUrlList.length();i++){
                imgList[i] =  imgUrlList.getString(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * http响应体的封装协议
     * @param <T> 泛型
     */
    public static class ResponseBody <T> {

        /**
         * 业务响应码
         */
        private int code;
        /**
         * 响应提示信息
         */
        private String msg;
        /**
         * 响应数据
         */
        private T data;

        public ResponseBody(){}

        public int getCode() {
            return code;
        }
        public String getMsg() {
            return msg;
        }
        public T getData() {
            return data;
        }

        @NonNull
        @Override
        public String toString() {
            sign = code;
            return "ResponseBody{" +
                    "code=" + code +
                    ", msg='" + msg + '\'' +
                    ", data=" + data +
                    '}';
        }
    }
}


//public class img_upload {
//
//    public  void uploadFile() throws IOException {
//
//        OkHttpClient client = new OkHttpClient();
//        String url="http://47.107.52.7:88/member/photo/image/upload";//上传URL
//        File file=new File("file:///data/c.png");//文件路径(本地)
//        MultipartBody.Builder requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
//        RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        String filename = file.getName();
//        requestBody.addFormDataPart("fileList", filename, body);
//        // 请求头
//        Headers headers = new Headers.Builder()
//                    .add("Accept", "application/json, text/plain, */*")
//                    .add("appId", appId)
//                    .add("appSecret", appSecret)
//                    .add("Content-Type", "multipart/form-data")
//                    .build();
//        Request request = new Request.Builder()
//                .url(url)
//                .headers(headers)
//                .post(requestBody.build())
//                .build();
//        //请求超时时间5s
//        client.newBuilder().readTimeout(5000, TimeUnit.MILLISECONDS).build().newCall(request);
//        final Call call = client.newCall(request);
//        Log.e("",request+"--"+call);
//        Response response = call.execute();
//        Log.e("response.body().string()--",response.body().string());
//
//    }
//}
