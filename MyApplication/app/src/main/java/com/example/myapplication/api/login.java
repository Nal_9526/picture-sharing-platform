package com.example.myapplication.api;

import static com.example.myapplication.api_basic.Basic.appId;
import static com.example.myapplication.api_basic.Basic.appSecret;

import androidx.annotation.NonNull;
import android.os.NetworkOnMainThreadException;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class login {
    public static final Gson gson = new Gson();

    public static int sign=0;
    public String username;
    public String password;
    public static String id;
    public static String name,introduce,avatar,lastUpdateTime;
    public login(String username,String password){
        this.username = username;
        this.password = password;
    }

    public void post(){
        new Thread(() -> {
        // url路径
        String url = "http://47.107.52.7:88/member/photo/user/login?password="+password+"&username="+username;//?password=string&username=string";

        // 请求头
        Headers headers = new Headers.Builder()
        .add("Accept", "application/json, text/plain, */*")
        .add("appId", appId)
        .add("appSecret", appSecret)
        .add("Content-Type", "application/json")
        .build();


        MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

        //请求组合创建
        Request request = new Request.Builder()
        .url(url)
        // 将请求头加至请求中
        .headers(headers)
        .post(RequestBody.create(MEDIA_TYPE_JSON, ""))
        .build();
        try {
        OkHttpClient client = new OkHttpClient();
        //发起请求，传入callback进行回调
        client.newCall(request).enqueue(callback);
        }catch (NetworkOnMainThreadException ex){
        ex.printStackTrace();
        }
        }).start();
    }

    /**
     * 回调
     */
    public final Callback callback = new Callback() {
        @Override
        public void onFailure(@NonNull Call call, IOException e) {
                //TODO 请求失败处理
                e.printStackTrace();
                }
        @Override
        public void onResponse(@NonNull Call call, Response response) throws IOException {
                //TODO 请求成功处理
                Type jsonType = new TypeToken<ResponseBody<Object>>(){}.getType();
                // 获取响应体的json串
                String body = response.body().string();
                Log.d("info", body);
                parseJSONWithJSONObject(body);//解析SSON数据
                // 解析json串到自己封装的状态
                ResponseBody<Object> dataResponseBody = gson.fromJson(body,jsonType);
                Log.d("info", dataResponseBody.toString());
                }
    };


    public void parseJSONWithJSONObject(String jsonData) {//用JSONObect解析JSON数据
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            id = jsonObject1.getString("id");
            name = jsonObject1.getString("username");
            introduce = jsonObject1.getString("introduce");
            avatar = jsonObject1.getString("avatar");
            lastUpdateTime = jsonObject1.getString("lastUpdateTime");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * http响应体的封装协议
     * @param <T> 泛型
     */
    public static class ResponseBody <T> {

        /**
         * 业务响应码
         */
        private int code;
        /**
         * 响应提示信息
         */
        private String msg;
        /**
         * 响应数据
         */
        private T data;

        public ResponseBody(){}

        public int getCode() {
            return code;
        }
        public String getMsg() {
            return msg;
        }
        public T getData() {
            return data;
        }

        @NonNull
        @Override
        public String toString() {
            sign = code;
            Log.e("!!!!",""+sign);
            return "ResponseBody{" +
                    "code=" + code +
                    ", msg='" + msg + '\'' +
                    ", data=" + data +
                    '}';
        }
    }
}




