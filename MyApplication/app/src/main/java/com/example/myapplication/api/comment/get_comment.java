package com.example.myapplication.api.comment;

import static com.example.myapplication.api_basic.Basic.appId;
import static com.example.myapplication.api_basic.Basic.appSecret;

import androidx.annotation.NonNull;

import android.os.NetworkOnMainThreadException;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class get_comment{
    private final Gson gson = new Gson();
    private String shareId;
    public static ArrayList<ArrayList<String>> group;
    public static int sign=0;

    public get_comment(String shareId){
        this.shareId = shareId;
        get();
    }

    private void get(){
        new Thread(() -> {

            // url路径
            String url = "http://47.107.52.7:88/member/photo/comment/first?shareId="+shareId;

            // 请求头
            Headers headers = new Headers.Builder()
                    .add("appId", appId)
                    .add("appSecret", appSecret)
                    .add("Accept", "application/json, text/plain, */*")
                    .build();

            //请求组合创建
            Request request = new Request.Builder()
                    .url(url)
                    // 将请求头加至请求中
                    .headers(headers)
                    .get()
                    .build();
            try {
                OkHttpClient client = new OkHttpClient();
                //发起请求，传入callback进行回调
                client.newCall(request).enqueue(callback);
            }catch (NetworkOnMainThreadException ex){
                ex.printStackTrace();
            }
        }).start();
    }

    /**
     * 回调
     */
    private final Callback callback = new Callback() {
        @Override
        public void onFailure(@NonNull Call call, IOException e) {
            //TODO 请求失败处理
            e.printStackTrace();
        }
        @Override
        public void onResponse(@NonNull Call call, Response response) throws IOException {
            //TODO 请求成功处理
            Type jsonType = new TypeToken<ResponseBody<Object>>(){}.getType();
            // 获取响应体的json串
            String body = response.body().string();
            Log.d("info", body);
            // 解析json串到自己封装的状态
            ResponseBody<Object> dataResponseBody = gson.fromJson(body,jsonType);
            parseJSONWithJSONObject(body);
            Log.d("info", dataResponseBody.toString());
        }
    };

    public void parseJSONWithJSONObject(String jsonData) {//用JSONObect解析JSON数据
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            JSONArray records = jsonObject1.getJSONArray("records");   //得到键为results的JSONArray
            String total = jsonObject1.getString("total");
            group = new ArrayList<ArrayList<String>>();
            for(int i=0;i<Integer.valueOf(total).intValue();i++){
                ArrayList<String> child = new ArrayList<String>();
                String id = records.getJSONObject(i).getString("id");
                String content = records.getJSONObject(i).getString("content");
                String username = records.getJSONObject(i).getString("userName");
                String shareId = records.getJSONObject(i).getString("shareId");
                String pUserId = records.getJSONObject(i).getString("pUserId");
                String parentCommentId = records.getJSONObject(i).getString("parentCommentId");
                String parentCommentUserId = records.getJSONObject(i).getString("parentCommentUserId");
                String replyCommentId = records.getJSONObject(i).getString("replyCommentId");
                String replyCommentUserId = records.getJSONObject(i).getString("replyCommentUserId");
                child.add(id);  //0
                child.add(content); //1
                child.add(username);    //2
                child.add(shareId); //3
                child.add(pUserId); //4
                child.add(parentCommentId); //5
                child.add(parentCommentUserId); //6
                child.add(replyCommentId);  //7
                child.add(replyCommentUserId);  //8
                child.add("1");
                group.add(child);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * http响应体的封装协议
     * @param <T> 泛型
     */
    public static class ResponseBody <T> {

        /**
         * 业务响应码
         */
        private int code;
        /**
         * 响应提示信息
         */
        private String msg;
        /**
         * 响应数据
         */
        private T data;

        public ResponseBody(){}

        public int getCode() {
            return code;
        }
        public String getMsg() {
            return msg;
        }
        public T getData() {
            return data;
        }

        @NonNull
        @Override
        public String toString() {
            sign = code;
            return "ResponseBody{" +
                    "code=" + code +
                    ", msg='" + msg + '\'' +
                    ", data=" + data +
                    '}';
        }
    }
}