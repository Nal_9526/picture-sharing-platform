package com.example.myapplication.api.user;

import static com.example.myapplication.api_basic.Basic.appId;
import static com.example.myapplication.api_basic.Basic.appSecret;

import android.os.NetworkOnMainThreadException;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class user_follow {
    private static final Gson gson = new Gson();
    private String current,userId;
    private ArrayList<ArrayList<String>> follow_group;

    public user_follow(String current, String userId){
        this.current = current;
        this.userId = userId;
        get();
    }

    public ArrayList<ArrayList<String>> get_info(){
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return follow_group;
    }

    private void get(){
        new Thread(() -> {
            // url路径
            String url = "http://47.107.52.7:88/member/photo/collect?current="+current+"&userId="+userId;
            // 请求头
            Headers headers = new Headers.Builder()
                    .add("appId", appId)
                    .add("appSecret", appSecret)
                    .add("Accept", "application/json, text/plain, */*")
                    .build();
            //请求组合创建
            Request request = new Request.Builder()
                    .url(url)
                    // 将请求头加至请求中
                    .headers(headers)
                    .get()
                    .build();
            try {
                OkHttpClient client = new OkHttpClient();
                //发起请求，传入callback进行回调
                client.newCall(request).enqueue(callback);
            }catch (NetworkOnMainThreadException ex){
                ex.printStackTrace();
            }
        }).start();
    }

    /**
     * 回调
     */
    private final Callback callback = new Callback() {
        @Override
        public void onFailure(@NonNull Call call, IOException e) {
            //TODO 请求失败处理
            e.printStackTrace();
        }
        @Override
        public void onResponse(@NonNull Call call, Response response) throws IOException {
            //TODO 请求成功处理
            Type jsonType = new TypeToken<ResponseBody<Object>>(){}.getType();
            // 获取响应体的json串
            String body = response.body().string();
            parseJSONWithJSONObject(body);//解析SSON数据
            Log.d("收藏表info", body);
            // 解析json串到自己封装的状态
            ResponseBody<Object> dataResponseBody = gson.fromJson(body,jsonType);
            Log.d("收藏表info", dataResponseBody.toString());
        }
    };

    public void parseJSONWithJSONObject(String jsonData) {//用JSONObect解析JSON数据
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            Log.d("疑问1",""+jsonObject);
            Object object = jsonObject.get("data");
            Log.d("user_follow-data",""+object);
            if(object==null){
                follow_group = null;
            }else {
                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                Log.d("疑问2",""+jsonObject1);
                JSONArray records = jsonObject1.getJSONArray("records");   //得到键为results的JSONArray
                Log.d("疑问3",""+records);
                // 收藏总数
                String total = jsonObject1.getString("total");
                // 每页数量
                String size = jsonObject1.getString("size");
                follow_group = new ArrayList<ArrayList<String>>();
                for(int i=0;i<Integer.valueOf(size);i++){
                    String collectId = records.getJSONObject(i).getString("collectId");
                    String content = records.getJSONObject(i).getString("content");
                    String createTime = records.getJSONObject(i).getString("createTime");
                    String imageCode = records.getJSONObject(i).getString("imageCode");
                    String imageUrlList = (String) records.getJSONObject(i).getJSONArray("imageUrlList").get(0);
                    String pUserId = records.getJSONObject(i).getString("pUserId");
                    String title = records.getJSONObject(i).getString("title");
                    String username = records.getJSONObject(i).getString("username");
                    String likeNum = records.getJSONObject(i).getString("likeNum");
                    String id = records.getJSONObject(i).getString("id");
                    ArrayList<String> follow_chlid = new ArrayList<String>();
                    follow_chlid.add(collectId); //0
                    follow_chlid.add(content); //1
                    follow_chlid.add(createTime); //2
                    follow_chlid.add(imageCode); //3
                    follow_chlid.add(imageUrlList); //4
                    follow_chlid.add(pUserId); //5
                    follow_chlid.add(title); //6
                    follow_chlid.add(username); //7
                    follow_chlid.add(total); //8
                    follow_chlid.add(size); //9
                    follow_chlid.add(likeNum); //10
                    follow_chlid.add(id); //11
                    follow_group.add(follow_chlid);
                }
                Log.e("收藏表-follow_group",""+follow_group);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * http响应体的封装协议
     * @param <T> 泛型
     */
    public static class ResponseBody <T> {

        /**
         * 业务响应码
         */
        private int code;
        /**
         * 响应提示信息
         */
        private String msg;
        /**
         * 响应数据
         */
        private T data;

        public ResponseBody(){}

        public int getCode() {
            return code;
        }
        public String getMsg() {
            return msg;
        }
        public T getData() {
            return data;
        }

        @NonNull
        @Override
        public String toString() {
            return "ResponseBody{" +
                    "code=" + code +
                    ", msg='" + msg + '\'' +
                    ", data=" + data +
                    '}';
        }
    }
}