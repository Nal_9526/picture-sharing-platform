package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.myapplication.fragement.AddFragment;
import com.example.myapplication.fragement.HomeFragment;
import com.example.myapplication.fragement.IndexFragment;

public class FunctionActivity extends AppCompatActivity implements View.OnClickListener {
    public static TextView tv1_index;
    public static TextView tv2_add;
    public static TextView tv3_home;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction,fragmentTransaction2;
    public static IndexFragment indexFragment;
    public static AddFragment addFragment;
    HomeFragment homeFragment;
//    Boolean bl = false;
    private LinearLayout function_bottom;
    private Context context = FunctionActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_function);

        initView();
    }

    private void initView() {
        tv1_index = findViewById(R.id.tv1_index);
        tv2_add = findViewById(R.id.tv2_add);
        tv3_home = findViewById(R.id.tv3_home);

        tv1_index.setBackgroundResource(R.drawable.ic_baseline_insert_photo_24_selected);

        RelativeLayout my1 = findViewById(R.id.my1);
        RelativeLayout my2 = findViewById(R.id.my2);
        RelativeLayout my3 = findViewById(R.id.my3);
        function_bottom = findViewById(R.id.function_bottom);
//        function_bottom.setBackgroundColor(Color.parseColor("#000000"));//黑色


        indexFragment = new IndexFragment();
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        //这里导包需要注意，Fragment有两个包，需要的是import android.app.Fragment;
        fragmentTransaction.replace(R.id.main_content,indexFragment);
        fragmentTransaction.commit();


        my1.setOnClickListener(this);
        my2.setOnClickListener(this);
        my3.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        fragmentTransaction2 = fragmentManager.beginTransaction();
        switch (v.getId()){
            case R.id.my1:
                //隐藏
                if(addFragment!=null){
                    fragmentTransaction2.hide(addFragment);
                }
                if(homeFragment!=null){
                    fragmentTransaction2.hide(homeFragment);
                }
                //展示
                if(indexFragment==null){
                    indexFragment = new IndexFragment();
                    fragmentTransaction2.add(R.id.main_content,indexFragment);
                }else{
                    fragmentTransaction2.show(indexFragment);
                }
                changecolor(tv1_index,R.drawable.ic_baseline_insert_photo_24_selected);
//                Toast.makeText(this, "首页", Toast.LENGTH_SHORT).show();
                break;
            case R.id.my2:
                if(indexFragment!=null){
                    fragmentTransaction2.hide(indexFragment);
                }
                if(homeFragment!=null){
                    fragmentTransaction2.hide(homeFragment);
                }

                if(addFragment==null){
                    addFragment = new AddFragment(context);
                    fragmentTransaction2.add(R.id.main_content,addFragment);
                }else{
                    // 清空所有残留信息
                    addFragment.clean();
                    fragmentTransaction2.show(addFragment);
                }
                changecolor(tv2_add,R.drawable.ic_baseline_add_24_selected);
//                Toast.makeText(this, "添加", Toast.LENGTH_SHORT).show();
                break;
            case R.id.my3:
                if(indexFragment!=null){
                    fragmentTransaction2.hide(indexFragment);
                }
                if(addFragment!=null){
                    fragmentTransaction2.hide(addFragment);
                }

                if(homeFragment==null){
                    homeFragment = new HomeFragment();
                    fragmentTransaction2.add(R.id.main_content,homeFragment);
                }else{
                    fragmentTransaction2.show(homeFragment);
                }
                changecolor(tv3_home,R.drawable.ic_baseline_home_24_selected);
//                Toast.makeText(this, "我的", Toast.LENGTH_SHORT).show();
                break;
        }
        fragmentTransaction2.commit();
    }

    public static void changecolor(TextView changeTextView,int changeId){
        //全部设为原有颜色
        tv1_index.setBackgroundResource(R.drawable.ic_baseline_insert_photo_24_white);
        tv2_add.setBackgroundResource(R.drawable.ic_baseline_add_24_white);
        tv3_home.setBackgroundResource(R.drawable.ic_baseline_home_24_white);

        //仅改变现点击的图标颜色
        changeTextView.setBackgroundResource(changeId);
    }

//    public void changeBottomcolor(){
//        this.bl = !bl;
//        if(bl == true) function_bottom.setBackgroundColor(Color.parseColor("#000000"));
//        else function_bottom.setBackgroundColor(Color.parseColor("#ffffff"));
//    }
}