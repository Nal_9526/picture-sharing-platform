package com.example.myapplication.api.content;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class getBitmapFromURL {
    private   Bitmap bitmap = null;
    private String path;
    private Context context;
    private String[] info = new String[]{"保存","分享","其它"};

    private Handler mhandler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg){
            //Log.e("msg.obj",""+msg.obj);
            bigImageLoader((Bitmap) msg.obj);
        }
    };

    public getBitmapFromURL(String path, Context context){
        this.path = path;
        this.context = context;

        URL url = null;
        try {
            url = new URL(this.path);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        requestImg(url);
        final Dialog dialog = new Dialog(context);
//        imageView.setImageBitmap(bitmap);
    }

    private void requestImg(final URL imgUrl)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Message message = new Message();
                    bitmap = BitmapFactory.decodeStream(imgUrl.openStream());
                    message.obj = bitmap;
                    mhandler.sendMessage(message);
                    //Log.e("bitmap-",""+bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void bigImageLoader(Bitmap bitmap) {
        final Dialog dialog = new Dialog(context);
        ImageView image = new ImageView(context);
        image.setImageBitmap(bitmap);
        dialog.setContentView(image);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
        image.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.cancel();
            }
        });
        //大图的长按监听
        image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //弹出的“保存图片”的Dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setItems(info, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(i==0){
                            saveCroppedImage(bitmap);
                        }else if(i==1){
                            URL url = null;
                            try {
                                url=new URL(path);
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("image/png");
                            intent.putExtra(Intent.EXTRA_STREAM, url);
                            context.startActivity(Intent.createChooser(intent,"分享"));
                        }else {
                            final AlertDialog ad = new AlertDialog.Builder(context).setMessage(
                                    "逗你玩呢，小屁孩...").show();
                        }
                    }
                });
                builder.show();
                return true;
            }
        });
    }
    // 保存图片
    private void saveCroppedImage(Bitmap bmp) {
        File file = new File( "/storage/emulated/0/Pictures"+ "/" + System.currentTimeMillis() + ".jpg");
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        final AlertDialog ad = new AlertDialog.Builder(context).setMessage(
                "哥哥~ 保存成功啦~").show();
        Log.e("长按-","保存图片成功"+file.getPath());
    }

}
