package com.example.myapplication.home;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.api.content.getBitmapFromURL;
import com.example.myapplication.api.user.follow;
import com.example.myapplication.api.user.unfollow;
import com.example.myapplication.api.user.unlike;
import com.example.myapplication.api.user.user_follow;
import com.example.myapplication.api.user.user_like;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Follow_Adapter extends BaseExpandableListAdapter {
    Context context;
    ArrayList<ArrayList<String>> mGroupList, follow_group;
    String[][] mChildList;//二级List 注意!这里是List里面套了一个List<String>,实际项目你可以写一个pojo类来管理2层数据

    //点赞记录-收藏记录
    private int[] num = new int[100];
    private int[] num1 = new int[100];
    public Follow_Adapter(ArrayList<ArrayList<String>> groupList, String[][] childList, Context mcontext){
        mGroupList = groupList;
        mChildList = childList;
        context = mcontext;
    }

    @Override
    public int getGroupCount() {//返回第一级List长度
        return  mGroupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {//返回指定groupPosition的第二级List长度
        return  mChildList.length;
    }

    @Override
    public Object getGroup(int groupPosition) {//返回一级List里的内容
        return mGroupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {//返回二级List的内容
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {//返回一级View的id 保证id唯一
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {//返回二级View的id 保证id唯一
        return childPosition;
    }

    /**
     * 指示在对基础数据进行更改时子ID和组ID是否稳定
     * @return
     */
    @Override
    public boolean hasStableIds() {
        return true;
    }

    /**
     *  返回一级父View
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        //加载布局文件
        ViewHolder viewHolder;
        View view;
        if(convertView == null){
            view = View.inflate(context, R.layout.home_follow_list,null);
            viewHolder = new ViewHolder();
            viewHolder.iv = view.findViewById(R.id.iv);
            viewHolder.iv_title = view.findViewById(R.id.iv_title);
            viewHolder.iv_name = view.findViewById(R.id.iv_name);
            viewHolder.iv_content = view.findViewById(R.id.iv_content);
            viewHolder.iv_comment_num = view.findViewById(R.id.iv_comments_num);
            viewHolder.iv_content_img = view.findViewById(R.id.iv_content_img);
            viewHolder.iv_time = view.findViewById(R.id.iv_time);
            viewHolder.iv_Thumbs = view.findViewById(R.id.iv_Thumbs);
            viewHolder.iv_comments = view.findViewById(R.id.iv_comments);
            viewHolder.iv_collection = view.findViewById(R.id.iv_collection);
            view.setTag(viewHolder);
        }else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        Log.e("mGroupList.get(groupPosition).get(4)",""+mGroupList.get(groupPosition).get(4));
        //list = mGroupList.get(groupPosition).get(4);
        Picasso.get().load(mGroupList.get(groupPosition).get(4))
                //.resizeDimen(R.dimen.image_width,R.dimen.image_height)
                .fit()
                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .error(R.drawable.ic_launcher_background)
                .into(viewHolder.iv_content_img);
        Picasso.get().load(mGroupList.get(groupPosition).get(4))
                .fit()
                .centerCrop()
                .into(viewHolder.iv);

        viewHolder.iv_title.setText(mGroupList.get(groupPosition).get(6));
        viewHolder.iv_name.setText(mGroupList.get(groupPosition).get(7));
        viewHolder.iv_content.setText(mGroupList.get(groupPosition).get(1));

        // 评论人数
        viewHolder.iv_comment_num.setText(mGroupList.get(groupPosition).get(10));

        // 时间戳转换成时间 - 发布时间
        Long time = Long.valueOf(mGroupList.get(groupPosition).get(2));
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sd = sdf.format(time);
        viewHolder.iv_time.setText(sd);

//        // 显示头像和内容图片 -长按功能
//        viewHolder.iv_content_img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String img = mGroupList.get(groupPosition).get(4);
//                new getBitmapFromURL(img, context);
//            }
//        });
        // 点赞功能
        viewHolder.iv_Thumbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(num[groupPosition]==0){
                    viewHolder.iv_Thumbs.setBackgroundResource(R.drawable.ic_launcher_background_2);
                    num[groupPosition]=1;
                    user_like like = new user_like(mGroupList.get(groupPosition).get(0), MainActivity.id);
                    Log.e("点赞----",mGroupList.get(groupPosition).get(0)+"---"+MainActivity.id);
                }else{
                    viewHolder.iv_Thumbs.setBackgroundResource(R.drawable.ic_launcher_background);
                    num[groupPosition]=0;
                    unlike unlike = new unlike(mGroupList.get(groupPosition).get(0));
                    Log.e("取消点赞","");
                }
            }
        });
        // 收藏功能
        viewHolder.iv_collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(num1[groupPosition]==0){
                    viewHolder.iv_collection.setBackgroundResource(R.drawable.ic_launcher_background_2);
                    num1[groupPosition]=1;
                    follow follow = new follow(mGroupList.get(groupPosition).get(0), MainActivity.id);
                    Log.e("收藏----","");
                }else{
                    viewHolder.iv_collection.setBackgroundResource(R.drawable.ic_launcher_background);
                    num1[groupPosition]=0;
                    user_follow user_follow = new user_follow("0",MainActivity.id);
                    follow_group = user_follow.get_info();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String total = follow_group.get(0).get(8);
                            //String size = follow_group.get(0).get(9);
                            int n=0,count=0,num=1;
                            while(true){    //可能会卡崩溃，OOM错误，需要完善
                                if(follow_group.get(n).get(0).equals(mGroupList.get(groupPosition).get(0))){
                                    new unfollow(mGroupList.get(groupPosition).get(0));
                                    break;
                                }else{
                                    n++;count++;
                                    if(follow_group.get(n).get(9).equals(""+n)){ //翻页
                                        n = 0;
                                        user_follow user_follow = new user_follow(""+num,MainActivity.id);
                                        follow_group = user_follow.get_info();
                                        num++;
                                    }
                                    if(total.equals(""+count)){ //全部查找完并没有
                                        Toast.makeText(context, "寻找失败！", Toast.LENGTH_SHORT).show();
                                        break;
                                    }
                                }
                            }
                        }
                    }).start();
                    Log.e("取消收藏","");
                }
            }
        });
        // 评论功能
        viewHolder.iv_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 弹出输入法
                input_box();
            }
        });

        return view;
    }

    private void input_box() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("评论");
        builder.setIcon(R.drawable.ic_launcher_background);
        builder.setMessage("请输入内容");
        final EditText editText = new EditText(context);
        builder.setView(editText);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String content = editText.getText().toString();
                Toast.makeText(context, "您输入了" + content, Toast.LENGTH_SHORT).show();
                if(content.equals("")){
                    Toast.makeText(context, "评论不能为空！", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(context, "评论成功！", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("取消", null);
        builder.show();
    }

    /**
     *  返回二级子View
     */
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        //加载布局文件
        View view = View.inflate(parent.getContext(), R.layout.item_comment,null);
        TextView name = (TextView) view.findViewById(R.id.comment_name);
        TextView content = (TextView) view.findViewById(R.id.comment_content);
//
//        name.setText(mChildList[childPosition][0]);
//        content.setText(mChildList[childPosition][1]);
//        get_comment g_c = new get_comment(mGroupList.get(groupPosition).get(0));
//        g_c.get();
//        while(g_c.sign==0) {
//            if (g_c.sign != 0) {break;}
//        }
//        Log.e("====",""+g_c.group);

//        if(g_c.sign==200 && g_c.group.size() != 0){
//            if(g_c.group.get(childPosition).get(2)!=null){
//                String na_me = g_c.group.get(childPosition).get(2);
//                String con_tent = g_c.group.get(childPosition).get(1);
//                name.setText(na_me);
//                content.setText(con_tent);
//            }
//        }

        return view;
    }

    /**
     * 便于GC回收
     */
    class ViewHolder{
        ImageView iv,iv_Thumbs,iv_comments,iv_content_img,iv_collection;
        TextView name,content,iv_name,iv_content,iv_time,iv_comment_num,iv_title;
    }

    /**
     *  指定位置的子项是否可选
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
