package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.api.user.modify_user_information;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

public class ChangeInformation extends AppCompatActivity implements View.OnClickListener {
    private ActionBar actionbar;
    private Context context = ChangeInformation.this;

    private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
    private static final int PHOTO_REQUEST_CUT = 3;// 结果

    private ImageView user_iv;
    private EditText user_name;
    private EditText user_introduce;
    private Button btn_changeinfo;

    private String url_string;
    private String UserName;
    private String UserIntroduce;

    public static ArrayList<Object> userinfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_information);

        init(); //初始化
        back(); //返回键
    }

    public static ArrayList<Object> get_userinfo(){
        return userinfo;
    }

    private void init(){
        this.user_iv = findViewById(R.id.user_iv);
        this.user_name = findViewById(R.id.user_name);
        this.user_introduce = findViewById(R.id.user_introduce);
        this.btn_changeinfo = findViewById(R.id.btn_changeinfo);
        this.btn_changeinfo.setOnClickListener(this);
        this.user_iv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_iv:     //更换图标
                Toast.makeText(context, "更换图标", Toast.LENGTH_SHORT).show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        gallery();
                    }
                }).start();
                break;
            case R.id.btn_changeinfo:    //提交
                UserName = user_name.getText().toString();
                UserIntroduce = user_introduce.getText().toString();
                Log.e("UserName-UserIntroduce",UserName+"-"+UserIntroduce);
                userinfo = new ArrayList<Object>();
                userinfo.add(this.url_string);
                userinfo.add(UserIntroduce);
                userinfo.add(UserName);
                Log.d("userinfo",""+userinfo);
                new modify_user_information(MainActivity.id,this.url_string, UserIntroduce, UserName);
                Toast.makeText(context, "提交成功", Toast.LENGTH_SHORT).show();
                Log.d("提交","成功");
                break;
            default:
                break;
        }
    }

    /*
     * 从相册获取
     */
    public void gallery() {
        // 激活系统图库，选择一张图片
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        //全文件搜索
//        intent.setAction(Intent.ACTION_GET_CONTENT);
        // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_GALLERY
        startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
    }
    /*
     * 剪切图片
     */
    private void crop(Uri uri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 250);
        intent.putExtra("outputY", 250);

        intent.putExtra("outputFormat", "JPEG");// 图片格式
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);
        // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CUT
        startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PHOTO_REQUEST_GALLERY) {
            // 从相册返回的数据
            if (data != null) {
                // 得到图片的全路径
                Uri uri = data.getData();
                Log.e("uri",""+uri);
                //this.url_string = uri.getPath().toString();
               // Log.e("url_string",""+url_string);
                // 将url转为文件格式
                String[] proj = { MediaStore.Images.Media.DATA };
                Cursor actualimagecursor = managedQuery(uri,proj,null,null,null);
                int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                actualimagecursor.moveToFirst();
                String img_path = actualimagecursor.getString(actual_image_column_index);
                File Fi = new File(img_path);
                Uri fileUri = Uri.fromFile(Fi);
                Log.e("Fi-",""+Fi);
                Log.e("fileUri-",""+fileUri);
                this.url_string = fileUri.toString();
                Log.e("url_string",""+url_string);
                crop(uri);
            }
        } else if (requestCode == PHOTO_REQUEST_CUT) {
            // 从剪切图片返回的数据
            if (data != null) {
                Bitmap bitmap = data.getParcelableExtra("data");
                //this.url_string = bitmap.toString();
                this.user_iv.setImageBitmap(bitmap);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    // 返回操作
    private void back(){
        actionbar = getSupportActionBar();
        actionbar.show();
        //显示返回箭头
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("修改用户信息");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:   //返回键的id
                this.finish();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}